##Domemo Project
Comparison of programming languages by Domemo implementation.

The objective is to implement and learn by various programming languages.

We welcome participation of those who can implement in the new programming language or 
those who can do better implementation!

==================================================================

This software is released under the MIT License, see LICENSE.txt.